import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { Branch } from './Branch';
import { BodyDto } from './DTO/BodyDto';
import { ResponseDto } from './DTO/ResponseDto';
import { Pagination } from './Pagination';
import { Transformer } from './transformer.decorator';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  async getHello(
    @Body() bodyDto: BodyDto
  ) {
    const response = await this.appService.getHello(bodyDto);
    // map response to api
    const apiResponse: Branch[] & Pagination = response;
    const x = [ { name: 'name1' },  { name: 'name2' }, {limit: 1} ]
    console.log(apiResponse);
    
    return apiResponse;
  }
}

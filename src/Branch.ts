export class Branch {
    name: string;
    constructor(data: Partial<Branch>) {
        return Object.assign(this, data);
    }
}

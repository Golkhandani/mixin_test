import { Injectable } from '@nestjs/common';
import { AppRepository } from './app.repository';
import { BodyDto } from './DTO/BodyDto';
import { ResponseDao } from './DTO/ResponseDao';
import { Pagination } from './Pagination';
import { Transformer } from './transformer.decorator';


@Injectable()
export class AppService {
  constructor(
    private readonly appRepository:AppRepository
    ){}

  // @Transformer()
  async getHello(
   bodyDto: BodyDto
  ): Promise<any> {
    // Transform BodyDTO to Body DAO

    const helloJohnny = await this.appRepository.helloJohnny(bodyDto as any);
    console.log(helloJohnny);
    
    return {helloJohnny, pagination:new Pagination({limit:helloJohnny.limit,offset:helloJohnny.offset})};
  }
}

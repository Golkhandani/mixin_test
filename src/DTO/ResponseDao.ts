import { Expose } from "class-transformer";

export class ResponseDao {
  @Expose()
  helloWorld: string;
  @Expose()
  helloJohnny: string | number;
}

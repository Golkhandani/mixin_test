// const updateSchema = (target: any, key: string | symbol, options: any): void => {
//     const s = Reflect.getMetadata(SCHEMA_KEY, target) || {};
//     s[key] = options;
//     Reflect.defineMetadata(SCHEMA_KEY, s, target);

// };
import { plainToClass } from "class-transformer"
import { ResponseDao } from "./DTO/ResponseDao";
import { ResponseDto } from "./DTO/ResponseDto";

export const Transformer = () => {
        return (target: any, key: string | symbol, descriptor?: PropertyDescriptor): any => {
            console.log("target",target);
            console.log("key",key);
            console.log("descriptor",descriptor);  
            console.log("descriptor.value",descriptor.value.toString());   
            var paramtypes = Reflect.getMetadata("design:paramtypes", target, key);
            var returntype = Reflect.getMetadata("design:returntype", target, key);
            console.log(`${key.toString()} type: ${paramtypes.name}`); 
            console.log(`${key.toString()} type: ${returntype.name}`);  
            
            const oldFunction = descriptor.value;

            descriptor.value = function (...args:any[]) {
                console.log("Function");
                const result = oldFunction.apply(this, args)
                const transformed = plainToClass(returntype, result, { excludeExtraneousValues: true, enableImplicitConversion: true });

                console.log(transformed instanceof ResponseDao);
                console.log(transformed);
                
                return transformed;
            }
        };
};
import { Injectable } from '@nestjs/common';
import { POINT_CONVERSION_HYBRID } from 'constants';
import { Branch } from './Branch';
import { BodyDao } from './DTO/BodyDao';
import { Pagination } from './Pagination';


@Injectable()
export class AppRepository {
    constructor() {}
    
    helloWorld(obj: BodyDao): string {
        return "Hello World"
    }
    async helloJohnny(obj: BodyDao): Promise<Branch[] & Pagination> {
        const b1 = new Branch({name:"name1"});
        const b2 = new Branch({name:"name2"});
        const b = [b1,b2];

        const p = new Pagination({limit:1,offset:2});
        const r = Object.assign(b,p)
            
        
        console.log(r);
        
        return  r;
    }
}
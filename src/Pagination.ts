export class Pagination {
    limit: number;
    offset: number;
    constructor(data: Partial<Pagination>) {
        return Object.assign(this, data);
    }
}
